package ru.t1.gorodtsova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.model.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.model.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.model.IProjectTaskService;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.TaskNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.TaskIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeOneById(task.getId());
        projectRepository.removeOneById(userId, projectId);
    }

}
