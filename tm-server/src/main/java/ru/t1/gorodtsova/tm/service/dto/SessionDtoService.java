package ru.t1.gorodtsova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.ISessionDtoService;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return repository.getSize(userId);
    }

}
