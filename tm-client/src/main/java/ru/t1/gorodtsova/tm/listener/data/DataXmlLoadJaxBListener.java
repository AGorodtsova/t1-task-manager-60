package ru.t1.gorodtsova.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlLoadJaxBRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadJaxBListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from xml file";

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataXmlLoadJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        domainEndpoint.loadDataXmlJaxb(request);
    }

}
